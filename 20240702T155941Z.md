To split a pane vertically in tmux, you can use the following key combination:

Ctrl + b, %

This will split the current pane into two vertical panes.
# Tue  2 Jul 15:59:41 CEST 2024 - tmux split pane vertically?